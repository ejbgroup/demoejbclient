package com.ejbclient;

import com.ejbsample.HelloWorld;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

/**
 * Hello world!
 *
 */
public class EJBClient {
    private Context context = null;

    public void createInitialContext() throws NamingException {
        Properties prop = new Properties();
        prop.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        prop.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.remote.client.InitialContextFactory");
        prop.put(Context.PROVIDER_URL, "http-remoting://127.0.0.1:8081");
        prop.put(Context.SECURITY_PRINCIPAL, "admin");
        prop.put(Context.SECURITY_CREDENTIALS, "admin");
        prop.put("jboss.naming.client.ejb.context", false);

        context = new InitialContext(prop);
    }

    public HelloWorld lookup() throws NamingException {
        String toLookup = "ejbsample-1.0-SNAPSHOT/HelloWorld!com.ejbsample.HelloWorld";
        return (HelloWorld) context.lookup(toLookup);
    }

    public String getEJBRemoteMessage() {
        try {
            // 1. Obtaining Context
            createInitialContext();
            // 2. Generate JNDI Lookup name and caste
            HelloWorld helloWorld = lookup();
            return helloWorld.getHelloWorld();
        } catch (NamingException e) {
            e.printStackTrace();
            return "";
        } finally {
            try {
                closeContext();
            } catch (NamingException e) {
                e.printStackTrace();
            }
        }
    }

    public void closeContext() throws NamingException {
        if (context != null) {
            context.close();
        }
    }

    public static void main(String args[]){
        EJBClient ejbClient = new EJBClient();
        System.out.println(ejbClient.getEJBRemoteMessage());
    }
}